//package generation;
import java.util.*;
import java.text.DecimalFormat;
/* this is the number generator that is used in the Gui
the calculations of the operations are made here too  */


public class Gen {
    private static DecimalFormat df = new DecimalFormat("#.##");// getting an simple decimal format by using the DecimalFormat(#.##) it set to 2 decimal points
    // variables
    int number1;
    int number2;
    double divNumber1;
    double divNumber2;
    int answer;
    double divAnswer;
    public Gen(int type){
        Random rand = new Random();// random will be used to generate the numbers
        if(type == 4){// here it checks if the type is 4 (division) so it can set the values to double based variables
            double number1Gen = (double) rand.nextInt(999);// double generations
            double number2Gen = (double) rand.nextInt(999);
            if(number1Gen > number2Gen){// checking whitch one is bigger
                divNumber1 = number1Gen;
                divNumber2 = number2Gen;
            }else{
                divNumber1 = number2Gen;
                divNumber2 = number1Gen;
            }
        }else{
            int number1Gen = rand.nextInt(999);// integer generation
            int number2Gen = rand.nextInt(999);
            if(number1Gen > number2Gen){// checking whitch one is bigger
                number1 = number1Gen;
                number2 = number2Gen;
            }else{
                number1 = number2Gen;
                number2 = number1Gen;
            }
        }
        if(type == 1){// addition
            answer = number1 + number2;
        }
        if(type == 2){// subtraction
            answer = number1 - number2; 
        }
        if(type == 3){// multiplication
            answer = number1 * number2;
        }
        if(type == 4){// division
            divAnswer = divNumber1 / divNumber2;

        }
    }
    // here are all the methods use in the other classes to get the numbers and the answer.
	public int getFirstNumber(){
        return(number1);
    }
    public int getSecondNumber(){
        return(number2);
    }
    public String getDivFirstNumber(){
        return(Double.toString(divNumber1));
    }
    public String getDivSecondNumber(){
        return(Double.toString(divNumber2));
    }
    public String getAnswer(){
        return(Integer.toString(answer));
    }
    public String getDivAnswer(){
        return(df.format(divAnswer));
    }
}
