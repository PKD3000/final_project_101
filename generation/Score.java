//package generation;
/* this is the Class that takes care of the score */
public class Score {
    int scoreValue;
    public Score(){

    }
    // this function is for the other classes to get the score
    public int getScore(){
        return(scoreValue);
    }
    // this function is for adding 1 to the score
    public void addScore(){
        scoreValue++;
    }
}