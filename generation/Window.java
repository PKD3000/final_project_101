/*
* Core file, contains GUI elements necessary to make the problem
* giver work properly. This program uses Java's JavaFX library 
* in order to allow for GUI creation and manipulation of values.
* This program was designed to let the user select 10 operations that
* he wants to do and compiles a score for the user once he is done with the exerices.
* Only the first button will be commented, as the rest are the same.
* This program is licensed under the GNU GPLv3 License. Please give us credit for our work :)
* Conceptualized, debugged and implemented by Ron Friedman (1926133) & Dustin Gawlowski () @ Vanier College
* May 10th 2020
*/


// Importing libraries & packages
//package generation;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Window extends Application {
    int count = 0;
    Score scoreUser = new Score();
    
    /** 
     * @param type
     * @return Gen
     */
    // this makes a sharable Gen type var in a function
    public Gen getGen(int type){
        Gen generation = new Gen(type);
        return(generation);
    }
    
    /** 
     * @return boolean
     */
    // this is only a check that comes true if the user did 10 operations
    public boolean finalCheck(){
        if(count == 10){
            return(true);
        }else{
            return(false);
        }
    }
    
    /** 
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        // Creating the starting buttons to allow the user to select the operation type
        Label welcome = new Label("You have 10 operations to do. Have fun!");
        Button op1 = new Button("Addition Problems"); 
        Button op2 = new Button("Subtraction Problems");
        Button op3 = new Button("Multiplication Problems");
        Button op4 = new Button("Division Problems");

        // Creation of VBox, a container to align and display various elements
        VBox root = new VBox(10, welcome, op1, op2, op3, op4);
        root.setSpacing(30); // Sets spacing (height) between each element
        root.setAlignment(Pos.BASELINE_CENTER); // Alignment type
         // Create the scene and set the root, width and height
        Scene scene = new Scene(root, 500,300);
        primaryStage.setScene(scene); // Set the scene
        primaryStage.setTitle("Choose your operation of choice");
        primaryStage.show(); // Display the scene to the user

        // When pressed, to the following 
        op1.setOnAction(a->{
            // Create new VBox for new window
            VBox op1Elements = new VBox();
            Gen generationAdd = getGen(1); 

            int num1 = generationAdd.getFirstNumber();
            int num2 = generationAdd.getSecondNumber();
            String answer = generationAdd.getAnswer();

            // Display to user 
            Label intro = new Label("Please solve the operation below and enter your answer in the text box: ");
            Label equation = new Label(num1 + " + " + num2 + " = ? ");
            op1Elements.getChildren().add(intro);
            op1Elements.getChildren().add(equation);
            TextField textField = new TextField();
            op1Elements.getChildren().add(textField);
            
            // Input validator/parser. It will not let the user input anything else but what is specified (no decimals)
            /** 
            * @param primaryStage
            * @return pattern.matcher
            */
            Pattern pattern = Pattern.compile("\\d*|\\d+\\,\\d*");
            TextFormatter formatter = new TextFormatter((UnaryOperator<TextFormatter.Change>) change -> {
                return pattern.matcher(change.getControlNewText()).matches() ? change : null;
            });

            // TextBox size parameters
            /** 
            * @param Width
            * 
            */
            textField.setMinWidth(50);
            textField.setPrefWidth(50);
            textField.setMaxWidth(400);
            textField.setTextFormatter(formatter);

            // Input confirmation buttons/UI navigation
            Button button2 = new Button("Go Back!");
            Button confirm = new Button("Confirm");
            op1Elements.getChildren().add(confirm);
            Button button3 = new Button("Try Another One!");
            Stage secondStage = new Stage();

            // confirm is pressed it passes the input and checks if it's right
            confirm.setOnAction(b->{
                count++;
                // if finalCheck is true it changes the window to the final scene where it displays the score and prompt the user to exit the program.
                if(finalCheck()){
                    Stage finalStage = new Stage();
                    Label clearText = new Label("Congratulations on finishing the exercices!");
                    Label results = new Label("Out of 10 questions, you have solved " + scoreUser.getScore());
                    VBox finalBox = new VBox(10, clearText, results);
                    finalBox.setAlignment(Pos.BASELINE_CENTER);
                    finalBox.setSpacing(30);
                    Scene finalScene = new Scene(finalBox, 500, 100);
                    finalStage.setScene(finalScene);
                    finalStage.setTitle("Complete!");
                    finalStage.show();
                    secondStage.close();
                }
                // deletes buttons and adds one... Gui manipulation
                op1Elements.getChildren().remove(button2);
                op1Elements.getChildren().remove(confirm);
                op1Elements.getChildren().add(button3);
                String inputted = textField.getText();
                // Input parser, this checks if the input entered matches the answer and outputs corresponding messages.
                if(inputted.equals(answer)) {
                    scoreUser.addScore();
                    Label right = new Label("Congrats! Your answer is correct!");
                    op1Elements.getChildren().add(right);
                    Label progress = new Label("You are now at " + count + "/10.");
                    op1Elements.getChildren().add(progress);
                } else {
                    Label wrong = new Label("Wrong answer. " + "Got " + inputted + ". " + 
                                            "The answer is " + answer + ". " + "Better luck next time!");
                    op1Elements.getChildren().add(wrong);
                    Label progress = new Label("You are now at " + count + "/10.");
                    op1Elements.getChildren().add(progress);
                }
            });
            Scene secondScene = new Scene(op1Elements, 500,500);
            // Window settings
            secondStage.setScene(secondScene); // set the scene
            secondStage.setTitle("Addition Operations");
            secondStage.show();
            primaryStage.close(); // close the first stage (Window)

            
            
            
            op1Elements.setSpacing(30);
            op1Elements.setAlignment(Pos.BASELINE_CENTER);
            op1Elements.getChildren().add(button2);
            button2.setOnAction(f->{
                primaryStage.setScene(scene);
                primaryStage.setTitle("Choose your preffered operation");
                primaryStage.show();
                secondStage.close();
            });
            button3.setOnAction(f->{
                primaryStage.setScene(scene);
                primaryStage.setTitle("Choose your preffered operation");
                primaryStage.show();
                secondStage.close();
            });
        
         });
        
        op2.setOnAction(c->{
            VBox op2Elements = new VBox();
            Gen generationSub = getGen(2);

            int num1 = generationSub.getFirstNumber();
            int num2 = generationSub.getSecondNumber();
            String answer = generationSub.getAnswer();

            Label intro = new Label("Please solve the operation below and enter your answer in the text box: ");
            Label equation = new Label(num1 + " - " + num2 + " = ? ");
            op2Elements.getChildren().add(intro);
            op2Elements.getChildren().add(equation);
            TextField textField = new TextField();
            op2Elements.getChildren().add(textField);
            
            Pattern pattern = Pattern.compile("\\d*|\\d+\\,\\d*");
            TextFormatter formatter = new TextFormatter((UnaryOperator<TextFormatter.Change>) change -> {
                return pattern.matcher(change.getControlNewText()).matches() ? change : null;
            });

            textField.setMinWidth(50);
            textField.setPrefWidth(50);
            textField.setMaxWidth(400);
            textField.setTextFormatter(formatter);


            Button button2 = new Button("Go Back!");
            Button confirm = new Button("Confirm");
            op2Elements.getChildren().add(confirm);
            Button button3 = new Button("Try Another One!");
            Stage secondStage = new Stage();

            confirm.setOnAction(b->{
                count++;
                if(finalCheck()){
                    Stage finalStage = new Stage();
                    Label clearText = new Label("Congratulations on finishing the exercices!");
                    Label results = new Label("Out of 10 questions, you have solved " + scoreUser.getScore());
                    VBox finalBox = new VBox(10, clearText, results);
                    finalBox.setAlignment(Pos.BASELINE_CENTER);
                    finalBox.setSpacing(30);
                    Scene finalScene = new Scene(finalBox, 500, 100);
                    finalStage.setScene(finalScene);
                    finalStage.setTitle("Complete!");
                    finalStage.show();
                    secondStage.close();
                }
                
                op2Elements.getChildren().remove(button2);
                op2Elements.getChildren().remove(confirm);
                op2Elements.getChildren().add(button3);
                String inputted = textField.getText();
                if(inputted.equals(answer)) {
                    scoreUser.addScore();
                    Label right = new Label("Congrats! Your answer is correct!");
                    op2Elements.getChildren().add(right);
                    Label progress = new Label("You are now at " + count + "/10.");
                    op2Elements.getChildren().add(progress);
                } else {
                    Label wrong = new Label("Wrong answer. " + "Got " + inputted + ". " + 
                                            "The answer is " + answer + ". " + "Better luck next time!");
                    op2Elements.getChildren().add(wrong);
                    Label progress = new Label("You are now at " + count + "/10.");
                    op2Elements.getChildren().add(progress);
                }
            });
            Scene secondScene = new Scene(op2Elements, 500,500);
            secondStage.setScene(secondScene); // set the scene
            secondStage.setTitle("Subtraction Operations");
            secondStage.show();
            primaryStage.close(); // close the first stage (Window)

            
            
            
            op2Elements.setSpacing(30);
            op2Elements.setAlignment(Pos.BASELINE_CENTER);
            op2Elements.getChildren().add(button2);
            button2.setOnAction(f->{
                primaryStage.setScene(scene);
                primaryStage.setTitle("Choose your preffered operation");
                primaryStage.show();
                secondStage.close();
            });
            button3.setOnAction(f->{
                primaryStage.setScene(scene);
                primaryStage.setTitle("Choose your preffered operation");
                primaryStage.show();
                secondStage.close();
            }); 
        });

        op3.setOnAction(c->{
            VBox op3Elements = new VBox();
            Gen generationSub = getGen(3);

            int num1 = generationSub.getFirstNumber();
            int num2 = generationSub.getSecondNumber();
            String answer = generationSub.getAnswer();

            Label intro = new Label("Please solve the operation below and enter your answer in the text box: ");
            Label equation = new Label(num1 + " * " + num2 + " = ? ");
            op3Elements.getChildren().add(intro);
            op3Elements.getChildren().add(equation);
            TextField textField = new TextField();
            op3Elements.getChildren().add(textField);
            
            Pattern pattern = Pattern.compile("\\d*|\\d+\\,\\d*");
            TextFormatter formatter = new TextFormatter((UnaryOperator<TextFormatter.Change>) change -> {
                return pattern.matcher(change.getControlNewText()).matches() ? change : null;
            });

            textField.setMinWidth(50);
            textField.setPrefWidth(50);
            textField.setMaxWidth(400);
            textField.setTextFormatter(formatter);


            Button button2 = new Button("Go Back!");
            Button confirm = new Button("Confirm");
            op3Elements.getChildren().add(confirm);
            Button button3 = new Button("Try Another One!");
            Stage secondStage = new Stage();

            confirm.setOnAction(b->{
                count++;
                if(finalCheck()){
                    Stage finalStage = new Stage();
                    Label clearText = new Label("Congratulations on finishing the exercices!");
                    Label results = new Label("Out of 10 questions, you have solved " + scoreUser.getScore());
                    VBox finalBox = new VBox(10, clearText, results);
                    finalBox.setAlignment(Pos.BASELINE_CENTER);
                    finalBox.setSpacing(30);
                    Scene finalScene = new Scene(finalBox, 500, 100);
                    finalStage.setScene(finalScene);
                    finalStage.setTitle("Complete!");
                    finalStage.show();
                    secondStage.close();
                }
                
                op3Elements.getChildren().remove(button2);
                op3Elements.getChildren().remove(confirm);
                op3Elements.getChildren().add(button3);
                String inputted = textField.getText();
                if(inputted.equals(answer)) {
                    scoreUser.addScore();
                    Label right = new Label("Congrats! Your answer is correct!");
                    op3Elements.getChildren().add(right);
                    Label progress = new Label("You are now at " + count + "/10.");
                    op3Elements.getChildren().add(progress);
                } else {
                    Label wrong = new Label("Wrong answer. " + "Got " + inputted + ". " + 
                                            "The answer is " + answer + ". " + "Better luck next time!");
                    op3Elements.getChildren().add(wrong);
                    Label progress = new Label("You are now at " + count + "/10.");
                    op3Elements.getChildren().add(progress);
                }
            });
            Scene secondScene = new Scene(op3Elements, 500,500);
            secondStage.setScene(secondScene); // set the scene
            secondStage.setTitle("Product Operations");
            secondStage.show();
            primaryStage.close(); // close the first stage (Window)

            
            
            
            op3Elements.setSpacing(30);
            op3Elements.setAlignment(Pos.BASELINE_CENTER);
            op3Elements.getChildren().add(button2);
            button2.setOnAction(f->{
                primaryStage.setScene(scene);
                primaryStage.setTitle("Choose your preffered operation");
                primaryStage.show();
                secondStage.close();
            });
            button3.setOnAction(f->{
                primaryStage.setScene(scene);
                primaryStage.setTitle("Choose your preffered operation");
                primaryStage.show();
                secondStage.close();
            }); 
        });

        op4.setOnAction(c->{
            VBox op4Elements = new VBox();
            Gen generationSub = getGen(4);

            String num1 = generationSub.getDivFirstNumber();
            String num2 = generationSub.getDivSecondNumber();
            String answer = generationSub.getDivAnswer();

            Label intro = new Label("Please solve the operation below and enter your answer in the text box: ");
            Label warning = new Label("This operation set accepts double values only and rounds the answer. ");
            Label warning2 = new Label("Please make sure to enter decimals even if there are none.");
            Label equation = new Label(num1 + " / " + num2 + " = ? ");
            op4Elements.getChildren().add(intro);
            op4Elements.getChildren().add(warning);
            op4Elements.getChildren().add(warning2);
            op4Elements.getChildren().add(equation);
            TextField textField = new TextField();
            op4Elements.getChildren().add(textField);
            
            Pattern pattern = Pattern.compile("\\d*(\\.\\d*)?");
            TextFormatter formatter = new TextFormatter((UnaryOperator<TextFormatter.Change>) change -> {
                return pattern.matcher(change.getControlNewText()).matches() ? change : null;
            });

            textField.setMinWidth(50);
            textField.setPrefWidth(50);
            textField.setMaxWidth(400);
            textField.setTextFormatter(formatter);


            Button button2 = new Button("Go Back!");
            Button confirm = new Button("Confirm");
            op4Elements.getChildren().add(confirm);
            Button button3 = new Button("Try Another One!");
            Stage secondStage = new Stage();

            confirm.setOnAction(b->{
                count++;
                if(finalCheck()){
                    Stage finalStage = new Stage();
                    Label clearText = new Label("Congratulations on finishing the exercices!");
                    Label results = new Label("Out of 10 questions, you have solved " + scoreUser.getScore());
                    VBox finalBox = new VBox(10, clearText, results);
                    finalBox.setAlignment(Pos.BASELINE_CENTER);
                    finalBox.setSpacing(30);
                    Scene finalScene = new Scene(finalBox, 500, 100);
                    finalStage.setScene(finalScene);
                    finalStage.setTitle("Complete!");
                    finalStage.show();
                    secondStage.close();
                }
                op4Elements.getChildren().remove(button2);
                op4Elements.getChildren().remove(confirm);
                op4Elements.getChildren().add(button3);
                String inputted = textField.getText();
                if(inputted.equals(answer)) {
                    scoreUser.addScore();
                    Label right = new Label("Congrats! Your answer is correct!");
                    op4Elements.getChildren().add(right);
                    Label progress = new Label("You are now at " + count + "/10.");
                    op4Elements.getChildren().add(progress);
                    
                } else {
                    Label wrong = new Label("Wrong answer. " + "Got " + inputted + ". " + 
                                            "The answer is " + answer + ". " + "Better luck next time!");
                    op4Elements.getChildren().add(wrong);
                    Label progress = new Label("You are now at " + count + "/10.");
                    op4Elements.getChildren().add(progress);
                }
            });
            Scene secondScene = new Scene(op4Elements, 500,500);
            secondStage.setScene(secondScene); // set the scene
            secondStage.setTitle("Division Operations");
            secondStage.show();
            primaryStage.close(); // close the first stage (Window)

            
            
            
            op4Elements.setSpacing(30);
            op4Elements.setAlignment(Pos.BASELINE_CENTER);
            op4Elements.getChildren().add(button2);
            button2.setOnAction(f->{
                primaryStage.setScene(scene);
                primaryStage.setTitle("Choose your preffered operation");
                primaryStage.show();
                secondStage.close();
            });
            button3.setOnAction(f->{
                primaryStage.setScene(scene);
                primaryStage.setTitle("Choose your preffered operation");
                primaryStage.show();
                secondStage.close();
            }); 
        });
    }

    public static void main(String[] args) {
        launch();
    }

}