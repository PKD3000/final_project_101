/* This is our test file for the Gen.java and the Score.java before using it in the GUI */
public class SimpleGen{

    public static void main(String[] args){
        Score score = new Score();// making new Score Object
        System.out.println(score.getScore());// printing the the Score while using the getScore() function from Score = 0
        Gen x = new Gen(2);// Creating a new Gen Object an passing 2 to Gen so we get a subtraction.
        score.addScore();// testing the score adding
        int number1 = x.getFirstNumber();// getting first generated number
        int number2 = x.getSecondNumber();// getting second generated number
        String answer = x.getAnswer();// getting the answer form the operation on the generated numbers
        System.out.println(answer);// printing the number
        System.out.println(score.getScore());// printing the the Score while using the getScore() function from Score = 1
    }
}
