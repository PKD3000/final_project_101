# Final_Project_101
# Introduction
This project is the solution to the problem that we chose for our final assignement as part of the Programming I class
at Vanier College. This project was submitted on Tuesday, May 12th 2020.
# Description
The project's aim is to provide the user with problems to solve in a easy-to-use GUI interface. The user can choose 
from 4 different operations, being addition, subtraction, multiplication and division by using buttons for means of
navigation through the program. The project was built using Java's JavaFX framework and its mulitple libraries. The user
needs to complete 10 operations of any kind, after which, the program outputs how many operations the user has completed
successfully. A reset function was not implemented. The

# Authors
The authors of this project are the following:
* Ron Friedman (1926133)
* Dustin Gawlowski (1842348)

# Division of Tasks
* Ron: 
    * Creation and implementation of the GUI component
    * Creation and implementation of buttons 
    * Styling of the GUI interface
* Dustin: 
    * Creation and implementation of randomizer functions
    * Creation and implementation of scoring system
    * Creation and implementation of solution testing functions

In general, while it can be observed that Ron mostly worked on the GUI element and Dustin on the generation element,
both worked on both of the elements.

# Challenges Encountered
* Ron:
    * Implementing button switching mechanics. For example, once you click confirm, the button was
    originally not able to be removed and switched to a single button. This caused issues with our
    vision of how the program should work but we found a solution in the end.
    * Implementing the window switching functionality as not easy at first, as I did not have the proper
    understanding of the libraries that I used. With a little bit of research, I quickly grasped the methods
    to use to do that.
    * Some aspects of styling were not easy to do, as there are many types of styling methods (VBox, GridPane, StackPane, etc.)
    and it was hard to determine which one was best to use. In the end, we chose VBox for its modularity and simplicity
    compared to the other ones.
    * Creating the parser was a challenge, as I did not really understand which keywords to put in order for the function to parse
    doubles and integers. This was solved by looking at the parser variables and implementing them according to what we needed.
    * Implementing the final window which displayed the score was a challenge, as we did not know how to transition once the count
    reached 10. This was solved by implementing a counting method  

* Dustin:
    * Implement class sharing was a big challenge as it did not work at first. The files were not able to interact with one another nor compile properly because each file missed dependencies from the other file, and vice versa. This was resolved
    by putting all the files in a separate folder and creating a package for all of them, which solved all of the above issues.
    * Figuring out the division operation was also a challenge, as the program only returned integers, while the division GUI element and we, were expecting the function to return decimal numbers. This was addressed by converting the generation tool
    to a double and creating separate variables for the division operator in order for that to work.
    * Implemeting the count inside the transition to score window function was a challenge, as I was confused as to why the elements from the previous window were displayed. This was easily fixed by creating a final checking function, which checks if the user has completed all 10 operations that were required, and then returns true or false. If true, it deletes all elements, else it continues running until the count value reaches 10.

# Compiling
This project was built using Visual Studio Code Release 1.44.2
Dependencies:
* java.util.function.UnaryOperator;
* java.util.regex.Pattern;
* javafx.application.Application;
* javafx.geometry.Pos;
* javafx.scene.Scene;
* javafx.scene.control.Button;
* javafx.scene.control.Label;
* javafx.scene.control.TextField;
* javafx.scene.control.TextFormatter;
* javafx.scene.layout.VBox;
* javafx.stage.Stage;

This project was built on Java 8 update 241 (1.8.0_241), with the SE Runtime version being 241-b07 (1.8.0_241-b07) and
the HotSpot 64-bit Server VM version being 241-b07 (25.241-b07, mixed mode) on Windows 10 Education version 1903 (18362.720).

This project was also built on OpenJDK version 14.0.1 (openjdk 14.0.1 2020-04-14), with the OpenJDK Runtime Environment being
build 14.0.01 (14.0.1+8) and the OpenJDK 64-bit Server VM version being 14.0.1 (14.0.1+8, mixed mode, sharing) on Debian GNU/Linux 10(buster), with the kernel version being 4.19.0-9-amd64 on x86-64.

To ensure accurate performance, it is recommended to use specifications similar to those listed above, else we are unable to guarantee that the program will work as expected.

To compile, please ensure that you have cloned the repository in its own directory. Navigate to ~/generation/ and run the following **in order**:
* `javac -cp . Window.java`
* `java Window`

Doing so should launch the GUI program. To exit, press the "X" on the top of the Window. Alternatively, you may also press
CTRL+C in the terminal window to terminate the program.

# Address to remote repository
The remote repository is hosted on GitLab and as such follows its [Terms of Service](https://about.gitlab.com/terms/) and [Privacy Policy](https://about.gitlab.com/privacy/).

The repository is available at the [following link](https://gitlab.com/PKD3000/final_project_101).
